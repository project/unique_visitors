(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.unique_visitors = {
    attach: function (context, settings) {
			
      $('#unique-visitors-form .form-actions input', context).click(function(){
        				
				var count = jQuery('#unique-visitors-form #edit-count-unique-visitors').val();
        if (count.length > 0) {					
					$(this).css('pointer-events','none');
					$(this).css('border', '5px solid #4caf50');
					$(this).prop("value", "Saving...");
				}				
      });			
    }
  };
})(jQuery, Drupal, drupalSettings);
