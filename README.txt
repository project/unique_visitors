+++++++++++++++
UNIQUE VISITORS
+++++++++++++++

- Display Unique Visitors Counter on the basis of unique IP Address
- Set initial value for Unique Visitors Counter
- Reset Unique Visitors Counter

+++++++++++++++++++++++++++++++++++++
HOW TO START UNIQUE VISITORS COUNTING
+++++++++++++++++++++++++++++++++++++

To make Unique Visitors module start counting then you need to:

1. Enable the Unique Visitors block, or
2. If you want to change the initial value of counting then
   - go to admin->configuration->Unique Visitors Counter