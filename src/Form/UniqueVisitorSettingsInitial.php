<?php
namespace Drupal\unique_visitors\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteBuilderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the form for filter Students.
 */
class UniqueVisitorSettingsInitial extends ConfigFormBase {

	/**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['unique_visitors.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'unique_visitors_form';
  }

    /**
   * Drupal\Core\Routing\RouteBuilder definition.
   *
   * @var \Drupal\Core\Routing\RouteBuilder
   */
  protected $routerBuilder;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Routing\RouteBuilderInterface $router_builder
   *   The route builder.
   */
  public function __construct(RouteBuilderInterface $router_builder) {
    $this->routerBuilder = $router_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('router.builder')
    );
  }


  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

		$form = parent::buildForm($form, $form_state);
		$config = $this->config('unique_visitors.settings');
						
    $form['count_unique_visitors'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Unique Visitor Counter Set'),
      '#description' => $this->t('Example: If we set value 500 then its starts from 501,502,...'),
      '#default_value' => $config->get('count_unique_visitors'),
			'#required' => TRUE,
		);
		
    $form['reset_visitors_counter'] = [
		   '#type' => 'checkbox',
			 '#title' => $this->t('Reset unique visitor counter'),
			 '#description' => $this->t('If we reset then counter start from zero(0)'),
			 '#default_value' => $config->get('reset_visitors_counter'),
    ];		

		$form['#attached']['library'][] = 'unique_visitors/unique_visitors.custom';

		return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
		
		$value = $form_state->getValue('count_unique_visitors');
		if(!is_numeric($value) || $value < 0){
			$form_state->setErrorByName('count_unique_visitors', t('The unique visitor counter set %counter is not valid.', array('%counter' => $value)));
      return;
		}		
		parent::validateForm($form, $form_state);
  }
  /**
   * {@inheritdoc}
   */
  public function submitForm(array & $form, FormStateInterface $form_state) {
		
    $config = $this->config('unique_visitors.settings');
		$unique_visitors_count = $form_state->getValue('count_unique_visitors');	
		if($form_state->getValue('reset_visitors_counter') == 1){
			$unique_visitors_count = 0;
			\Drupal::service('unique_visitors.unique_visitors_count_helper')->resetUniqueVisitorCounter();
		}		
    $config->set('count_unique_visitors', $unique_visitors_count);
    $config->save();
		
    // Set route rebuild.
    $this->routerBuilder->setRebuildNeeded();
    return parent::submitForm($form, $form_state);
  }
}
