<?php

namespace Drupal\unique_visitors\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a block for user visitors.
 *
 * @Block(
 *   id = "unique_user_visitors",
 *   admin_label = @Translation("Unique Visitors"),
 * )
 */
class VisitorsCountBlock extends BlockBase{
	
	/**
   * {@inheritdoc}
   */
  public function build() {
		
		\Drupal::service('page_cache_kill_switch')->trigger();
		
		$config = \Drupal::config('unique_visitors.settings');
    $count_start = $config->get('count_unique_visitors');
				
		$visitors_count = \Drupal::service('unique_visitors.unique_visitors_count_helper')->getUniqueVisitorCounts();
		
		$total_visitor = $count_start + $visitors_count;
		
		return [
      '#attached' => [
        'library' => array('unique_visitors/unique_visitors.custom'),
      ],		
		  '#theme' => 'visitorscount',
			'#visitors_count' => $total_visitor,
      '#cache' => [
        'max-age' => 0,
      ],
    ];
		
	}	
		
} 