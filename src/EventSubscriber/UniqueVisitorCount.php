<?php

namespace Drupal\unique_visitors\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class UniqueVisitorCount.
 *
 * @package Drupal\unique_visitors
 */
class UniqueVisitorCount implements EventSubscriberInterface {

  /**
   * @var
   */
  protected $account_proxy;

  /**
   * Constructor.
   */
  public function __construct() {
    $this->account_proxy = \Drupal::currentUser();
  }

	/**
		* Whenever the kernel.request event is dispatched then this method is called.
		*
		* @param Event $event
		*/
  public function onRequestCount(Event $event){
		
		$current_user = $this->account_proxy->getAccount();

		if($current_user->isAnonymous())
		{ 
      $host = \Drupal::request()->getSchemeAndHttpHost();
      $current_path = \Drupal::request()->getRequestUri();
			$visit_path	= $host.$current_path;

			if(!empty($_SERVER['HTTP_CLIENT_IP'])){ // Share Internet IP					
				$ip_address = $_SERVER['HTTP_CLIENT_IP'];
			}elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){ // Proxy IP					
				$ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
			}else{
				$ip_address = $_SERVER['REMOTE_ADDR'];
			}

      \Drupal::service('unique_visitors.unique_visitors_count_helper')->SaveUniqueVisitors($ip_address, $visit_path);
			
		  return;
		}
  }

  /**
   * {@inheritdoc}
   */
  static function getSubscribedEvents() {
		$events[KernelEvents::REQUEST][] = ['onRequestCount', 30];
    return $events;
  }
	
}