<?php

namespace Drupal\unique_visitors\Services;

use Drupal\Core\Database\Connection;

/**
 * UniqueVisitorCountService class.
 */
class UniqueVisitorCountService {
  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;
  /**
   * Construct a unique visitors object.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   */
  public function __construct(Connection $connection) {
    $this->connection = $connection;
  }
	
  /**
   * SAVE unique visitors IPAddress
   */
  public function SaveUniqueVisitors($currentIpAddress, $visit_path) {
				
    $query = $this->connection->select('unique_visitors', 'uv');
    $query->fields('uv',array('ip_address'));
    $query->condition('uv.ip_address',$currentIpAddress, '=');
    $query = $query->execute();
    $data = $query->fetchAll(\PDO::FETCH_OBJ);
		
		if(empty($data)){ // Insert Unique IP Address
			$result = $this->connection->insert('unique_visitors')
			->fields([
				'ip_address' => $currentIpAddress,
				'visit_path' => $visit_path,
				'visit_date' => \Drupal::time()->getRequestTime(),
			])
			->execute();			
		}
		
  }  
	
  /**
   * Return unique visitors count
   */
  public function getUniqueVisitorCounts() {
	
    $query = $this->connection->select('unique_visitors', 'uv');
    $query->fields('uv',array('ip_address'));
    $query = $query->execute();
    $data = $query->fetchAll();
		
    $total_visitor = count($data);    
		return $total_visitor;		
  } 	
	
  /**
   * Reset unique visitors count
   */
  public function resetUniqueVisitorCounter() {
		
		$this->connection->truncate('unique_visitors')->execute();
		return;
  } 	

}
